unit DigitalPersona.Types;

{ Copyright (c) 2015 Ortu Agustin, ortu.agustin@gmail.com
Copyright (c) 2015 Ortu Agustin, https://gitlab.com/u/ortuagustin
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of copyright holders nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL COPYRIGHT HOLDERS OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE. }

interface

uses
  Winapi.ActiveX;

const
  MAX_FINGER_COUNT = 10;

type
  TReaderPriority = (rpLow, rpNormal, rpHigh);

  TReaderPriorityHelper = record helper for TReaderPriority
  public
    class function FromOleEnum(const Value: TOleEnum): TReaderPriority; static; inline;
    class function ToString(const Value: TReaderPriority): string; overload; static; inline;
    class function ToOleEnunm(const Value: TReaderPriority): TOleEnum; overload; static; inline;
    function ToString: string; overload; inline;
    function ToOleEnunm: TOleEnum; overload; inline;
  end;

  TFinger = (LeftLittleFinger, LeftRingFinger, LeftMiddleFinger, LeftIndexFinger, LeftThumb, RightThumb,
    RightIndexFinger, RightMiddleFinger, RightRingFinger, RightLittleFinger);

  TFingerHelper = record helper for TFinger
  strict private const
    RIGHT_THUMB_FINGER = $1;
    RIGHT_INDEX_FINGER = $2;
    RIGHT_MIDDLE_FINGER = $3;
    RIGHT_RING_FINGER = $4;
    RIGHT_LITTLE_FINGER = $5;
    LEFT_LITTLE_FINGER = $6;
    LEFT_RING_FINGER = $7;
    LEFT_MIDDLE_FINGER = $8;
    LEFT_INDEX_FINGER = $9;
    LEFT_THUMB_FINGER = $10;
  public
    class function FromInteger(const Value: Integer): TFinger; static; inline;
    class function ToString(const Value: TFinger): string; overload; static; inline;
    class function ToInteger(const Value: TFinger): Integer; overload; static; inline;
    function ToString: string; overload; inline;
    function ToInteger: Integer; overload; inline;
  end;

  TFingerSet = set of TFinger;

  TFingerSetHelper = record helper for TFingerSet
  public
    class function FromInteger(const Value: Integer): TFingerSet; static; inline;
    class function ToInteger(const Value: TFingerSet): Integer; overload; static; inline;
    function ToInteger: Integer; overload; inline;
    function Count: Integer; inline;
  end;

  TFingerPrints = array [TFinger] of string;

implementation

uses
  DPFPDevXLib_TLB, System.SysUtils;

{ TReaderPriorityHelper }

class function TReaderPriorityHelper.ToString(const Value: TReaderPriority): string;
begin
  case Value of
    rpLow: Result := 'Low';
    rpNormal: Result := 'Normal';
    rpHigh: Result := 'High';
  end;
end;

class function TReaderPriorityHelper.ToOleEnunm(const Value: TReaderPriority): TOleEnum;
begin
  case Value of
    rpLow: Result := CapturePriorityLow;
    rpNormal: Result := CapturePriorityNormal;
    rpHigh: Result := CapturePriorityHigh;
  end;
end;

class function TReaderPriorityHelper.FromOleEnum(const Value: TOleEnum): TReaderPriority;
begin
  case Value of
    CapturePriorityLow: Result := rpLow;
    CapturePriorityNormal: Result := rpNormal;
    CapturePriorityHigh: Result := rpHigh;
  end;
end;

function TReaderPriorityHelper.ToOleEnunm: TOleEnum;
begin
  Result := ToOleEnunm(Self);
end;

function TReaderPriorityHelper.ToString: string;
begin
  Result := ToString(Self);
end;

{ TFingerHelper }

class function TFingerHelper.FromInteger(const Value: Integer): TFinger;
begin
  case Value of
    LEFT_LITTLE_FINGER: Result := LeftLittleFinger;
    LEFT_RING_FINGER: Result := LeftRingFinger;
    LEFT_MIDDLE_FINGER: Result := LeftMiddleFinger;
    LEFT_INDEX_FINGER: Result := LeftIndexFinger;
    LEFT_THUMB_FINGER: Result := LeftThumb;
    RIGHT_THUMB_FINGER: Result := RightThumb;
    RIGHT_INDEX_FINGER: Result := RightIndexFinger;
    RIGHT_MIDDLE_FINGER: Result := RightMiddleFinger;
    RIGHT_RING_FINGER: Result := RightRingFinger;
    RIGHT_LITTLE_FINGER: Result := RightLittleFinger;
  end;
end;

function TFingerHelper.ToInteger: Integer;
begin
  Result := ToInteger(Self);
end;

class function TFingerHelper.ToInteger(const Value: TFinger): Integer;
begin
  case Value of
    LeftLittleFinger: Result := LEFT_LITTLE_FINGER;
    LeftRingFinger: Result := LEFT_RING_FINGER;
    LeftMiddleFinger: Result := LEFT_MIDDLE_FINGER;
    LeftIndexFinger: Result := LEFT_INDEX_FINGER;
    LeftThumb: Result := LEFT_THUMB_FINGER;
    RightThumb: Result := RIGHT_THUMB_FINGER;
    RightIndexFinger: Result := RIGHT_INDEX_FINGER;
    RightMiddleFinger: Result := RIGHT_MIDDLE_FINGER;
    RightRingFinger: Result := RIGHT_RING_FINGER;
    RightLittleFinger: Result := RIGHT_LITTLE_FINGER;
  end;
end;

class function TFingerHelper.ToString(const Value: TFinger): string;
begin
  case Value of
    LeftLittleFinger: Result := 'LeftLittleFinger';
    LeftRingFinger: Result := 'LeftRingFinger';
    LeftMiddleFinger: Result := 'LeftMiddleFinger';
    LeftIndexFinger: Result := 'LeftIndexFinger';
    LeftThumb: Result := 'LeftThumb';
    RightThumb: Result := 'RightThumb';
    RightIndexFinger: Result := 'RightIndexFinger';
    RightMiddleFinger: Result := 'RightMiddleFinger';
    RightRingFinger: Result := 'RightRingFinger';
    RightLittleFinger: Result := 'RightLittleFinger';
  end;
end;

function TFingerHelper.ToString: string;
begin
  Result := ToString(Self);
end;

{ TFingerSetHelper }

function TFingerSetHelper.Count: Integer;
var
  LFinger: TFinger;
begin
  Result := 0;
  for LFinger := Low(TFinger) to High(TFinger) do
    if LFinger in Self then
      Inc(Result);
end;

class function TFingerSetHelper.FromInteger(const Value: Integer): TFingerSet;
var
  LIntSet: TIntegerSet;
  LByte: Byte;
begin
  LIntSet := TIntegerSet(Value);
  Result := [];
  for LByte in LIntSet do
    Include(Result, TFinger(LByte));
end;

class function TFingerSetHelper.ToInteger(const Value: TFingerSet): Integer;
var
  LIntSet: TIntegerSet;
  LFinger: TFinger;
begin
  LIntSet := [];
  for LFinger in Value do
    Include(LIntSet, Ord(LFinger));

  Result := Integer(LIntSet);
end;

function TFingerSetHelper.ToInteger: Integer;
begin
  Result := ToInteger(Self);
end;

end.
