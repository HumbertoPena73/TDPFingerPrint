unit DigitalPersona.FingerPrint.Hashes;

{ Copyright (c) 2015 Ortu Agustin, ortu.agustin@gmail.com
Copyright (c) 2015 Ortu Agustin, https://gitlab.com/u/ortuagustin
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. Neither the name of copyright holders nor the names of its
   contributors may be used to endorse or promote products derived
   from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
''AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL COPYRIGHT HOLDERS OR CONTRIBUTORS
BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE. }

interface

uses
  DCPrijndael;

type
  IFingerHasher = interface
    ['{F4847EFD-9898-4AFB-AFE1-A70373813C63}']
    function Encrypt(const FingerPrint: string): string;
    function Decrypt(const FingerPrint: string): string;
  end;

  TFingerHasher = class(TInterfacedObject, IFingerHasher)
  private
    FDCPrijndael: TDCP_rijndael;
  public
    constructor Create;
    destructor Destroy; override;
    function Encrypt(const FingerPrint: string): string;
    function Decrypt(const FingerPrint: string): string;
  end;

var
  FingerPrintHasher: IFingerHasher;

implementation

uses
  DCPsha256;

const
  ENCRYPTION_KEY = '12345678901234567890123456789012';

{ TFingerHasher }

constructor TFingerHasher.Create;
begin
  inherited Create;
  FDCPrijndael := TDCP_rijndael.Create(NIL);
end;

destructor TFingerHasher.Destroy;
begin
  FDCPrijndael.Free;
  inherited Destroy;
end;

function TFingerHasher.Encrypt(const FingerPrint: string): string;
begin
  FDCPrijndael.InitStr(ENCRYPTION_KEY, TDCP_sha256);
  Result := FDCPrijndael.EncryptString(FingerPrint);
  FDCPrijndael.Reset;
end;

function TFingerHasher.Decrypt(const FingerPrint: string): string;
begin
  FDCPrijndael.InitStr(ENCRYPTION_KEY, TDCP_sha256);
  Result := FDCPrijndael.DecryptString(FingerPrint);
  FDCPrijndael.Reset;
end;

initialization
  FingerPrintHasher := TFingerHasher.Create;

end.
